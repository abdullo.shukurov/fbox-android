package wd.ritm.fbox_billing.model.system

interface AuthHolder {
    var token: String?
    fun clear()
}