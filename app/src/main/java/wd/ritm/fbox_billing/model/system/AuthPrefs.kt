package wd.ritm.fbox_billing.model.system

import android.content.Context
import android.content.Context.MODE_PRIVATE

class AuthPrefs(private val context: Context) : AuthHolder {
    companion object {
        private const val PREFS_NAME = "app_prefs"
        private const val TOKEN = "token"
    }

    override var token: String?
        get() = getPrefs().getString(TOKEN, null)
        set(value) {
            getPrefs().edit().putString(TOKEN, value).apply()
        }

    private fun getPrefs() = context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE)

    override fun clear() {
        getPrefs().edit().clear().apply()
    }
}
